Please enter our `Account Number` found on your statement where circled below into the top most field.

Enter the amount you would like to pay in the `Payment Amount` field, thank you.

![Etactics image](/etactics-example.png)
