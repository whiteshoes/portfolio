function initPayPalButton(x, y) {
    paypal.Buttons({
      style: {
        shape: 'rect',
        color: 'gold',
        layout: 'vertical',
        label: 'paypal',
        
      },

      createOrder: function(data, actions) {
        return actions.order.create({
          purchase_units: [{
              amount: {
                  "currency_code":"USD",
                  "value": y
              },
              description: x
          }]
        });
      },

      onApprove: function(data, actions) {
        return actions.order.capture().then(function(orderData) {
          
          // Full available details
          console.log('Capture result', orderData, JSON.stringify(orderData, null, 2));

          // Show a success message within this page, e.g.
          const element = document.getElementById('paypal-button-container');
          element.innerHTML = '';
          element.innerHTML = '<h3>Thank you for your payment!</h3>';

          // Or go to another URL:  actions.redirect('thank_you.html');
          
        });
      },

      onError: function(err) {
        console.log(err);
      }
    }).render('#paypal-button-container');
}

function myFunction() {
    document.getElementById("priceLabelError").style.visibility = 'hidden';        
    document.getElementById("descriptionError").style.visibility = 'hidden';
    let x = document.getElementById("description").value;
    let y = document.getElementById("amount").value;
    let text;
    if (isNaN(y) || y < 3) {
        document.getElementById("priceLabelError").style.visibility = 'visible';        
    } else if (x.length < 8) {
        document.getElementById("descriptionError").style.visibility = 'visible';
    } else {
        document.getElementById("paynow").style.visibility = 'hidden';
        initPayPalButton(x, y);     
    }
}

  

